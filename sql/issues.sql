--
-- PostgreSQL database dump
--

-- Dumped from database version 11.7 (Ubuntu 11.7-0ubuntu0.19.10.1)
-- Dumped by pg_dump version 11.7 (Ubuntu 11.7-0ubuntu0.19.10.1)

SET search_path=parts2,public;

--
-- Name: issues; Type: TABLE; Schema: public; Owner: gitlab
--

CREATE TABLE parts2.issues (
    id integer NOT NULL,
    title character varying(510) DEFAULT NULL::character varying,
    author_id integer,
    project_id integer NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    description text,
    milestone_id integer,
    state character varying(510) DEFAULT NULL::character varying,
    iid integer,
    updated_by_id integer,
    weight integer,
    confidential boolean DEFAULT false NOT NULL,
    moved_to_id integer,
    due_date date,
    lock_version integer,
    title_html text,
    description_html text,
    time_estimate integer,
    relative_position integer,
    service_desk_reply_to character varying,
    cached_markdown_version integer,
    last_edited_at timestamp without time zone,
    last_edited_by_id integer,
    discussion_locked boolean,
    closed_at timestamp with time zone,
    closed_by_id integer,
    state_id smallint DEFAULT 1 NOT NULL,
    duplicated_to_id integer,
    promoted_to_epic_id integer,
    root_namespace_id integer
)
PARTITION BY HASH (root_namespace_id) ;


--
-- Name: issues_id_seq; Type: SEQUENCE; Schema: public; Owner: gitlab
--

CREATE SEQUENCE parts2.issues_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: issues_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: gitlab
--

ALTER SEQUENCE parts2.issues_id_seq OWNED BY parts2.issues.id;


--
-- Name: issues id; Type: DEFAULT; Schema: public; Owner: gitlab
--

ALTER TABLE parts2.issues ALTER COLUMN id SET DEFAULT nextval('parts2.issues_id_seq'::regclass);


--
-- Name: issues issues_pkey; Type: CONSTRAINT; Schema: public; Owner: gitlab
--

ALTER TABLE parts2.issues
    ADD CONSTRAINT issues_pkey PRIMARY KEY (root_namespace_id, id);


--
-- Name: idx_issues_on_project_id_and_created_at_and_id_and_state_id; Type: INDEX; Schema: public; Owner: gitlab
