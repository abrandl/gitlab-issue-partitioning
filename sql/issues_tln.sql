\timing

create index on namespaces (id, parent_id);
create index on namespaces (id) where parent_id is null;
 
drop table namespaces_with_root;

create table namespaces_with_root (id integer not null primary key, root_namespace_id  integer not null references namespaces (id));
CREATE INDEX ON namespaces_with_root (root_namespace_id);

INSERT INTO namespaces_with_root (id, root_namespace_id)
WITH RECURSIVE "base_and_descendants" AS (
(
                        SELECT
                            "namespaces".id, namespaces.id AS root_namespace_id
                        FROM
                            "namespaces"
                        WHERE
                            parent_id IS NULL
                            )
                    UNION (
                        SELECT
                            "namespaces".id, base_and_descendants.root_namespace_id
                        FROM
                            "namespaces",
                            "base_and_descendants"
                        WHERE
                            "namespaces"."parent_id" = "base_and_descendants"."id"))


SELECT * from base_and_descendants;

drop table issues_with_nsp;
create table issues_with_nsp as select issues.id, nwr.root_namespace_id FROM issues JOIN projects ON issues.project_id = projects.id LEFT JOIN namespaces_with_root nwr ON nwr.id = projects.namespace_id;

