alter table parts.issues alter constraint fk_05f1e72feb deferrable initially deferred;
alter table parts.issues alter constraint fk_899c8f3231 deferrable initially deferred;
alter table parts.issues alter constraint fk_96b1dd429c deferrable initially deferred;
alter table parts.issues alter constraint fk_c63cbf6c25 deferrable initially deferred;
alter table parts.issues alter constraint fk_df75a7c8b8 deferrable initially deferred;
alter table parts.issues alter constraint fk_ffed080f01 deferrable initially deferred;
alter table parts.issues alter constraint issues_root_namespace_id_fkey deferrable initially deferred;

