require 'sequel'
require 'pry'
require 'benchmark'
require 'open3'
require 'net/http'

class QueryBenchmark
  include Benchmark

  EXPLAIN_HOST = URI('https://explain.depesz.com')

  attr_reader :query, :repeats, :db

  def initialize(query, repeats: 5)
    @query = query
    @repeats = repeats
    connect
  end

  def connect
    @db.disconnect if @db
    @db = Sequel.connect('postgres://abrandl-gl:abrandl-gl@localhost/gitlabhq_production')
  end

  def explain(query)
    db["EXPLAIN (summary) #{query}"].map(:"QUERY PLAN").join("\n")
  end

  def flush_os_caches
    puts "Flushing OS caches and restarting PG"
    stdout, stderr, status = Open3.capture3("sudo ./flush_os_caches")

    puts stdout
    puts stderr

    raise "Failed to flush caches" unless status.success?
    connect
  end

  def execute(table)
    (0..63).each do |n|
      detach_all
      attach_up_to(n)

      analyze table

      result = repeats.times.map { benchmark(query) }
      puts "#{n},#{result.join(',')}"
    end
  end

  def analyze(table)
    db.execute("ANALYZE #{table}")
  end

  def attach_up_to(n)
    (0..n).each do |part|
      db.execute("ALTER TABLE parts2.issues ATTACH PARTITION parts2.issues_#{part} FOR VALUES WITH (modulus 64, remainder #{part})")
    end
  end

  def detach_all
    (0..63).each do |part|
      begin
        db.execute("ALTER TABLE parts2.issues DETACH PARTITION parts2.issues_#{part}")
      rescue => e
        raise unless e.to_s =~ /is not a partition/
      end
    end
  end

  def benchmark(query)
    plan = explain(query).strip
    planning_time, _ = plan.match(/Planning Time: ([0-9\.]+)/).captures

    planning_time
  end
end

project_id = 13083
namespace_id = 9970

new_query = <<~SQL
        SELECT
            "issues".*
        FROM
            parts2."issues"
            INNER JOIN "projects" ON "projects"."id" = "issues"."project_id"
            LEFT JOIN project_features ON projects.id = project_features.project_id
            LEFT OUTER JOIN milestones ON issues.milestone_id = milestones.id
        WHERE
            "projects"."namespace_id" IN ( WITH RECURSIVE "base_and_descendants" AS (
(
                        SELECT
                            "namespaces".*
                        FROM
                            "namespaces"
                        WHERE
                            "namespaces"."type" = 'Group'
                            AND "namespaces"."id" = 9970)
                    UNION (
                        SELECT
                            "namespaces".*
                        FROM
                            "namespaces",
                            "base_and_descendants"
                        WHERE
                            "namespaces"."type" = 'Group'
                            AND "namespaces"."parent_id" = "base_and_descendants"."id"))
                SELECT
                    "namespaces"."id"
                FROM
                    "base_and_descendants" AS "namespaces")
                AND (EXISTS (
                        SELECT
                            1
                        FROM
                            "project_authorizations"
                        WHERE
                            "project_authorizations"."user_id" = 1562869
                            AND (project_authorizations.project_id = projects.id)
                            AND (project_authorizations.access_level >= 10))
                        OR projects.visibility_level IN (10, 20))
                    AND ("project_features"."issues_access_level" IS NULL
                        OR "project_features"."issues_access_level" IN (20, 30)
                        OR ("project_features"."issues_access_level" = 10
                            AND EXISTS (
                                SELECT
                                    1
                                FROM
                                    "project_authorizations"
                                WHERE
                                    "project_authorizations"."user_id" = 1562869
                                    AND (project_authorizations.project_id = projects.id)
                                    AND (project_authorizations.access_level >= 10))))
                        AND ("issues"."state_id" IN (1))
                        AND "projects"."archived" = FALSE
                        AND "milestones"."title" = '12.7'
        AND ("issues"."title" ILIKE '%database%'
            OR "issues"."description" ILIKE '%database%')
    AND "projects"."archived" = FALSE
ORDER BY
    "issues"."created_at" DESC,
    "issues"."id" DESC
LIMIT 20 OFFSET 0
SQL

QueryBenchmark.new(new_query).execute("parts2.issues")
