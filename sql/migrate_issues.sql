\timing
INSERT INTO parts2.issues
SELECT i.*, root_namespace_id
FROM public.issues i
JOIN public.issues_with_nsp USING (id)
ORDER BY root_namespace_id;
