
\timing

--
-- Name: idx_issues_on_project_id_and_created_at_and_id_and_state_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX idx_issues_on_project_id_and_created_at_and_id_and_state_id ON parts2.issues USING btree (project_id, created_at, id, state_id);


--
-- Name: idx_issues_on_project_id_and_due_date_and_id_and_state_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX idx_issues_on_project_id_and_due_date_and_id_and_state_id ON parts2.issues USING btree (project_id, due_date, id, state_id) WHERE (due_date IS NOT NULL);


--
-- Name: idx_issues_on_project_id_and_due_date_and_id_and_state_partial; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX idx_issues_on_project_id_and_due_date_and_id_and_state_partial ON parts2.issues USING btree (project_id, due_date, id, state) WHERE (due_date IS NOT NULL);


--
-- Name: idx_issues_on_project_id_and_rel_position_and_state_id_and_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX idx_issues_on_project_id_and_rel_position_and_state_id_and_id ON parts2.issues USING btree (project_id, relative_position, state_id, id DESC);


--
-- Name: idx_issues_on_project_id_and_updated_at_and_id_and_state_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX idx_issues_on_project_id_and_updated_at_and_id_and_state_id ON parts2.issues USING btree (project_id, updated_at, id, state_id);


--
-- Name: idx_issues_on_state_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX idx_issues_on_state_id ON parts2.issues USING btree (state_id);


--
-- Name: index_issues_on_author_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_issues_on_author_id ON parts2.issues USING btree (author_id);


--
-- Name: index_issues_on_closed_by_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_issues_on_closed_by_id ON parts2.issues USING btree (closed_by_id);


--
-- Name: index_issues_on_confidential; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_issues_on_confidential ON parts2.issues USING btree (confidential);


--
-- Name: index_issues_on_description_trigram; Type: INDEX; Schema: public; Owner: gitlab
--



--
-- Name: index_issues_on_duplicated_to_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_issues_on_duplicated_to_id ON parts2.issues USING btree (duplicated_to_id) WHERE (duplicated_to_id IS NOT NULL);


--
-- Name: index_issues_on_milestone_id; Type: INDEX; Schema: public; Owner: gitlab
--

