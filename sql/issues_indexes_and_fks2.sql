
\timing
CREATE INDEX index_issues_on_milestone_id ON parts2.issues USING btree (milestone_id);


--
-- Name: index_issues_on_moved_to_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_issues_on_moved_to_id ON parts2.issues USING btree (moved_to_id) WHERE (moved_to_id IS NOT NULL);


--
-- Name: index_issues_on_project_id_and_created_at_and_id_and_state; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_issues_on_project_id_and_created_at_and_id_and_state ON parts2.issues USING btree (project_id, created_at, id, state);


--
-- Name: index_issues_on_project_id_and_iid; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE UNIQUE INDEX index_issues_on_project_id_and_iid ON parts2.issues USING btree (root_namespace_id, project_id, iid);


--
-- Name: index_issues_on_project_id_and_rel_position_and_state_and_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_issues_on_project_id_and_rel_position_and_state_and_id ON parts2.issues USING btree (project_id, relative_position, state, id DESC);


--
-- Name: index_issues_on_project_id_and_updated_at_and_id_and_state; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_issues_on_project_id_and_updated_at_and_id_and_state ON parts2.issues USING btree (project_id, updated_at, id, state);


--
-- Name: index_issues_on_promoted_to_epic_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_issues_on_promoted_to_epic_id ON parts2.issues USING btree (promoted_to_epic_id) WHERE (promoted_to_epic_id IS NOT NULL);


--
-- Name: index_issues_on_relative_position; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_issues_on_relative_position ON parts2.issues USING btree (relative_position);


--
-- Name: index_issues_on_state; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_issues_on_state ON parts2.issues USING btree (state);


--
-- Name: index_issues_on_title_trigram; Type: INDEX; Schema: public; Owner: gitlab
--


--
-- Name: index_issues_on_updated_at; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_issues_on_updated_at ON parts2.issues USING btree (updated_at);


--
-- Name: index_issues_on_updated_by_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_issues_on_updated_by_id ON parts2.issues USING btree (updated_by_id) WHERE (updated_by_id IS NOT NULL);


--
-- Name: issues_id_idx; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX issues_id_idx ON parts2.issues USING btree (id) WHERE (lock_version IS NULL);


