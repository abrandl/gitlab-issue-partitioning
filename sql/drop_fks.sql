alter table parts.issues drop constraint fk_05f1e72feb;
alter table parts.issues drop constraint fk_899c8f3231;
alter table parts.issues drop constraint fk_96b1dd429c
alter table parts.issues drop constraint fk_c63cbf6c25;
alter table parts.issues drop constraint fk_df75a7c8b8;
alter table parts.issues drop constraint fk_ffed080f01;
alter table parts.issues drop constraint issues_root_namespace_id_fkey;

alter table parts.issues drop index if exists
